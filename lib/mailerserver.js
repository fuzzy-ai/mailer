// mailerserver.coffee
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const assert = require('assert')
const fs = require('fs')
const path = require('path')

const Microservice = require('@fuzzy-ai/microservice')
const _ = require('lodash')
const async = require('async')
const email = require('emailjs')
const pug = require('pug')
const uuid = require('uuid')

const version = require('./version')
const HTTPError = require('./httperror')
const html2text = require('./html2text')

class MailerServer extends Microservice {
  environmentToConfig (env) {
    // Accept either new-style (no prefix) or old-style (prefixed) keys

    const penv = function (key) {
      if (_.has(env, key)) {
        return env[key]
      } else if (_.has(env, `MAILER_${key}`)) {
        return env[`MAILER_${key}`]
      } else {
        return undefined
      }
    }

    const cfg = super.environmentToConfig(env)

    cfg.mailSource = penv('MAIL_SOURCE') || path.join(__dirname, '../mail-src')
    cfg.mailHost = penv('MAIL_HOST')
    cfg.mailPort = penv('MAIL_PORT')
    cfg.mailUsername = penv('MAIL_USERNAME') || penv('MAIL_USER')
    cfg.mailPassword = penv('MAIL_PASSWORD')
    cfg.mailFrom = penv('MAIL_FROM') || 'support@fuzzy.ai'
    cfg.homeHost = penv('HOME_HOST') || 'fuzzy.ai'

    const hp = penv('HOME_PORT')

    cfg.homePort = hp ? parseInt(hp, 10) : 80

    return cfg
  }

  startCustom (callback) {
    this.email = email.server.connect({
      user: this.config.mailUsername,
      password: this.config.mailPassword,
      host: this.config.mailHost,
      port: this.config.mailPort
    })

    this.cache = {}

    const sendMessage = (message, callback) => {
      return this.email.send(message, (err, sent) => callback(err))
    }

    this.q = async.queue(sendMessage, 8)

    return callback(null)
  }

  setupParams (exp) {
    exp.param('templateName', this._templateParam.bind(this))
    exp.param('messageID', this._messageID.bind(this))
  }

  _templateParam (req, res, next, templateName) {
    req.templateName = templateName
    if (_.has(this.cache, templateName)) {
      req.template = this.cache[templateName]
      return next()
    } else {
      const ms = this.config.mailSource
      const lc = templateName.toLowerCase()
      const fullPath = path.join(ms, `${lc}.pug`)
      return fs.readFile(fullPath, 'utf-8', (err, data) => {
        if (err) {
          return next(new HTTPError(`Cannot read file ${templateName}`, 404))
        } else {
          let template
          try {
            template = pug.compile(data)
          } catch (error) {
            err = error
            return next(err)
          }
          req.template = (this.cache[templateName] = template)
          return next()
        }
      })
    }
  }

  _messageID (req, res, next, id) {
    req.messageID = id
    const messages = this.q.workersList()
    const message = _.find(messages, {id})
    if (message != null) {
      req.message = message
    } else {
      req.message = null
    }
    return next()
  }

  setupRoutes (exp) {
    exp.post('/:templateName', this._postTemplate.bind(this))

    exp.get('/message/:messageID', this._getMessage.bind(this))

    exp.get('/version', this._getVersion.bind(this))
    exp.get('/live', this._getLive.bind(this))
    return exp.get('/ready', this._getReady.bind(this))
  }

  _postTemplate (req, res, next) {
    let homeServer
    const { template } = req
    const {to, subject, data} = req.body
    if (!to || !subject || !data) {
      const msg = "'to', 'subject' and 'data' parameters are required"
      return next(new HTTPError(msg, 400))
    }
    const from = this.config.mailFrom

    const {homeHost, homePort} = this.config

    assert.ok(_.isString(homeHost))
    assert.ok(_.isFinite(homePort))

    if ((homePort === 80) || (homePort === 443)) {
      homeServer = homeHost
    } else {
      homeServer = `${homeHost}:${homePort}`
    }

    data.homeServer = homeServer

    if (homePort === 443) {
      data.homeProtocol = 'https'
    } else {
      data.homeProtocol = 'http'
    }

    const body = template(data)

    const textBody = html2text(body)

    const message = {
      id: uuid.v4(),
      text: textBody,
      from,
      to,
      subject,
      attachment: [
        {data: body, type: 'text/html', alternative: true}
      ]
    }

    this.q.push(message, (err) => {
      if (err) {
        return req.log.error({err}, 'Error sending email')
      } else {
        return req.log.info({
          to,
          subject,
          templateName: req.templateName,
          message: 'Email sent'
        })
      }
    })

    return res.status(202).json({
      status: 'Accepted',
      id: message.id,
      message: `Message ${message.id} accepted for sending.`
    })
  }

  _getMessage (req, res, next) {
    if (req.message != null) {
      return res.json(req.message)
    } else {
      return next(new HTTPError('Message completed or unknown', 404))
    }
  }

  _getVersion (req, res, next) {
    return res.json({name: 'mailer', version})
  }

  _getLive (req, res, next) {
    return res.json({status: 'OK'})
  }

  _getReady (req, res, next) {
    // FIXME: should probably check the mail server status
    return res.json({status: 'OK'})
  }

  startDatabase (callback) {
    return callback(null)
  }

  stopDatabase (callback) {
    return callback(null)
  }
}

module.exports = MailerServer
