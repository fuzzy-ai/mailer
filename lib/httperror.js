// httperror.coffee
// Copyright 2014 Evan Prodromou <evan@prodromou.name>
// All rights reserved.

class HTTPError extends Error {
  constructor (message, statusCode) {
    super(message)
    this.statusCode = statusCode
  }
}

module.exports = HTTPError
