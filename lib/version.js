// version.coffee
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const fs = require('fs')
const path = require('path')

const getVersion = function () {
  const pkg = fs.readFileSync(path.join(__dirname, '..', 'package.json'))
  const data = JSON.parse(pkg)
  return data.version
}

module.exports = getVersion()
