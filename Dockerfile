FROM node:9-alpine

RUN apk add --no-cache curl

WORKDIR /opt/mailer
COPY . /opt/mailer

RUN npm install

EXPOSE 80
EXPOSE 443

ENTRYPOINT ["/opt/mailer/bin/dumb-init", "--"]
CMD ["npm", "start"]
