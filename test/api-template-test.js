// mailerserver-test.coffee
// Copyright 2015 Evan Prodromou <evan@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const templateBatch = require('./template-batch')

const props = {
  to: 'fakeuser@unit.test',
  subject: 'This is a test email',
  data: {
    item1: 1,
    item2: 2
  }
}

vows
  .describe('POST /:templateName')
  .addBatch(templateBatch('test', props, {
    'message includes correct link' (err, message, body) {
      assert.ifError(err)
      assert.isObject(message)
      assert.isString(message.data)
      return assert.match(message.data, /http:\/\/fuzzyio.test:1516\//)
    }
  }
  )).export(module)
