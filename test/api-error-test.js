// mailerserver-test.coffee
// Copyright 2015 Evan Prodromou <evan@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')
const simplesmtp = require('simplesmtp')
const async = require('async')
const _ = require('lodash')

const MailerServer = require('../lib/mailerserver')

const EMAIL = 'developer-list@unit.test'
const HOSTNAME = 'api3.unit.test'
const APP = 'api'
const MESSAGE = 'A sample error happened'
const USER = 'fakename@other.test'
const TIMESTAMP = '2015-05-30T00:00:00Z'
const URL = 'https://api.fuzzy.ai/agents'
const UNIQUE = '0e406f78-0ddf-11e5-8149-c8f73398600c'

const oneEmail = function (smtp, addr, callback) {
  let data = null
  let timeoutID = null
  const isOurs = envelope => _.includes(envelope.to, addr)
  const starter = function (envelope) {
    if (isOurs(envelope)) {
      data = ''
      smtp.on('data', accumulator)
      smtp.once('dataReady', ender)
    }
  }
  const accumulator = function (envelope, chunk) {
    if (isOurs(envelope)) {
      data = data + chunk.toString()
    }
  }
  const ender = function (envelope, cb) {
    let msg = null
    if (isOurs(envelope)) {
      clearTimeout(timeoutID)
      smtp.removeListener('data', accumulator)
      msg = _.clone(envelope)
      msg.data = data
      callback(null, msg)
      process.nextTick(() => cb(null))
    }
  }
  const to = () => callback(new Error('Timeout waiting for email'), null)

  timeoutID = setTimeout(to, 5000)

  smtp.on('startData', starter)
}

process.on('uncaughtException', (err) => {
  console.log('Uncaught exception')
  return console.error(err)
})

const errToJSON = function (err) {
  const props = {
    message: err.message,
    name: err.name
  }
  for (const name in err) {
    const value = err[name]
    props[name] = value
  }
  return props
}

vows
  .describe('POST /error')
  .addBatch({
    'When we configure a fake email server': {
      topic () {
        const smtp = simplesmtp.createServer({disableDNSValidation: true})
        smtp.listen(1623, err => {
          if (err) {
            return this.callback(err)
          } else {
            return this.callback(null, smtp)
          }
        })
        return undefined
      },
      'it works' (err, smtp) {
        assert.ifError(err)
        return assert.isObject(smtp)
      },
      'teardown' (smtp) {
        const { callback } = this
        return smtp.end(() => callback(null))
      },
      'and we start the mailer server': {
        topic () {
          const { callback } = this
          try {
            const env = {
              PORT: 2342,
              HOSTNAME: 'localhost',
              LOG_FILE: '/dev/null',
              MAIL_HOST: 'localhost',
              MAIL_PORT: 1623,
              MAIL_FROM: 'noreply@fuzzy.ai',
              APP_KEY_TESTAPPKEY: 'testappkey',
              HOME_HOST: 'fuzzyio.test',
              HOME_PORT: 1516
            }
            const server = new MailerServer(env)
            server.start((err) => {
              if (err) {
                return callback(err)
              } else {
                return callback(null, server)
              }
            })
          } catch (error) {
            const err = error
            callback(err)
            return
          }
          return undefined
        },
        'it works' (err, server) {
          return assert.ifError(err)
        },
        'teardown' (server) {
          const { callback } = this
          return server.stop(() => callback(null))
        },
        'and we request a template': {
          topic (mailer, smtp) {
            const { callback } = this
            const err = new Error(MESSAGE)
            err.statusCode = 500
            err.unique = UNIQUE
            const options = {
              url: 'http://localhost:2342/error',
              json: {
                to: EMAIL,
                subject: 'Error',
                data: {
                  hostname: HOSTNAME,
                  app: APP,
                  error: errToJSON(err),
                  timestamp: TIMESTAMP,
                  extra: {
                    user: USER,
                    url: URL
                  }
                }
              },
              headers: {
                authorization: 'Bearer testappkey'
              }
            }
            async.parallel([
              callback => oneEmail(smtp, EMAIL, callback),
              callback =>
                request.post(options, (err, response, body) => {
                  const code = response.statusCode
                  if (err) {
                    return callback(err)
                  } else if ((code >= 400) && (code <= 599)) {
                    const msg = body != null ? body.message : undefined
                    return callback(new Error(`Bad status code ${code}: ${msg}`))
                  } else {
                    return callback(null, body)
                  }
                })

            ], (err, results) => {
              if (err) {
                return callback(err)
              } else {
                const [message, body] = Array.from(results)
                return callback(null, message, body)
              }
            })
            return undefined
          },
          'it works' (err, message, body) {
            assert.ifError(err)
            assert.isObject(body)
            assert.isString(body.status)
            return assert.equal('Accepted', body.status)
          },
          'message is sent' (err, message, body) {
            assert.ifError(err)
            assert.isObject(message)
            assert.isString(message.data)
            assert.match(message.data, new RegExp(MESSAGE))
            assert.match(message.data, new RegExp(UNIQUE))
            assert.match(message.data, new RegExp(HOSTNAME))
            assert.match(message.data, new RegExp(APP))
            assert.match(message.data, new RegExp(TIMESTAMP))
            assert.match(message.data, new RegExp(USER))
            return assert.match(message.data, new RegExp(URL))
          }
        }
      }
    }}).export(module)
