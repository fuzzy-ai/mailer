// mailerserver-test.coffee
// Copyright 2015 Evan Prodromou <evan@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')
const async = require('async')
const _ = require('lodash')

const apiBatch = require('./api-batch')

const oneEmail = function (smtp, addr, callback) {
  let data = null
  let timeoutID = null
  const isOurs = envelope => _.includes(envelope.to, addr)
  const starter = function (envelope) {
    if (isOurs(envelope)) {
      data = ''
      smtp.on('data', accumulator)
      smtp.once('dataReady', ender)
    }
  }
  const accumulator = function (envelope, chunk) {
    if (isOurs(envelope)) {
      data = data + chunk.toString()
    }
  }
  const ender = function (envelope, cb) {
    let msg = null
    if (isOurs(envelope)) {
      clearTimeout(timeoutID)
      smtp.removeListener('data', accumulator)
      msg = _.clone(envelope)
      msg.data = data
      callback(null, msg)
      process.nextTick(() => cb(null))
    }
  }
  const to = () => callback(new Error('Timeout waiting for email'), null)

  timeoutID = setTimeout(to, 5000)

  return smtp.on('startData', starter)
}

const templateBatch = function (template, json, rest) {
  const templateRequest = {
    [`and we request the ${template} template`]: {
      topic (mailer, smtp) {
        const { callback } = this
        const options = {
          url: `http://localhost:2342/${template}`,
          json,
          headers: {
            authorization: 'Bearer testappkey'
          }
        }
        async.parallel([
          callback => oneEmail(smtp, json.to, callback),
          callback =>
            request.post(options, (err, response, body) => {
              const code = response != null ? response.statusCode : undefined
              if (err) {
                return callback(err)
              } else if ((code >= 400) && (code <= 599)) {
                const message = body != null ? body.message : undefined
                return callback(new Error(`Bad status code ${code}: ${message}`))
              } else {
                return callback(null, body)
              }
            })

        ], (err, results) => {
          if (err) {
            return callback(err)
          } else {
            const [message, body] = Array.from(results)
            return callback(null, message, body)
          }
        })
        return undefined
      },
      'it works' (err, message, body) {
        assert.ifError(err)
        assert.isObject(body)
        assert.isString(body.status)
        return assert.equal('Accepted', body.status)
      },
      'message is sent' (err, message, body) {
        assert.ifError(err)
        assert.isObject(message)
        return assert.isString(message.data)
      }
    }
  }

  if (rest != null) {
    const c = `and we request the ${template} template`
    _.assign(templateRequest[c], rest)
  }

  return apiBatch(templateRequest)
}

module.exports = templateBatch
