// mailerserver-test.coffee
// Copyright 2015 Evan Prodromou <evan@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const simplesmtp = require('simplesmtp')
const _ = require('lodash')

const MailerServer = require('../lib/mailerserver')

const apiBatch = function (rest) {
  const base = {
    'When we configure a fake email server': {
      topic () {
        const smtp = simplesmtp.createServer({disableDNSValidation: true})
        smtp.listen(1623, err => {
          if (err) {
            return this.callback(err)
          } else {
            return this.callback(null, smtp)
          }
        })
        return undefined
      },
      'it works' (err, smtp) {
        assert.ifError(err)
        return assert.isObject(smtp)
      },
      'teardown' (smtp) {
        const { callback } = this
        return smtp.end(() => callback(null))
      },
      'and we start the mailer server': {
        topic () {
          const { callback } = this
          try {
            const env = {
              PORT: 2342,
              HOSTNAME: 'localhost',
              LOG_FILE: '/dev/null',
              MAIL_HOST: 'localhost',
              MAIL_PORT: 1623,
              MAIL_FROM: 'noreply@fuzzy.ai',
              APP_KEY_TESTAPPKEY: 'testappkey',
              HOME_HOST: 'fuzzyio.test',
              HOME_PORT: 1516
            }
            const server = new MailerServer(env)
            server.start((err) => {
              if (err) {
                return callback(err)
              } else {
                return callback(null, server)
              }
            })
          } catch (error) {
            const err = error
            callback(err)
            return
          }
          return undefined
        },
        'it works' (err, server) {
          return assert.ifError(err)
        },
        'teardown' (server) {
          const { callback } = this
          server.stop(() => callback(null))
          return undefined
        }
      }
    }
  }

  if (rest != null) {
    const a = 'When we configure a fake email server'
    const b = 'and we start the mailer server'
    _.assign(base[a][b], rest)
  }
  return base
}

module.exports = apiBatch
