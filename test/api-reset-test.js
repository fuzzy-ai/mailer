// mailerserver-test.coffee
// Copyright 2015, 2016 Evan Prodromou <evan@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const templateBatch = require('./template-batch')

const EMAIL = 'passwordreset@unit.test'
const CODE = '1234567890'

const props = {
  to: EMAIL,
  subject: 'This is a test email',
  data: {
    confirmation: {
      code: CODE
    }
  }
}

vows
  .describe('POST /reset')
  .addBatch(templateBatch('reset', props, {
    'the confirmation URL is included' (err, message, body) {
      assert.ifError(err)
      const rx = new RegExp(`http://fuzzyio.test:1516/reset-confirm/${CODE}`)
      return assert.match(message != null ? message.data : undefined, rx)
    }
  }
  )).export(module)
