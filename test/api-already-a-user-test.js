// mailerserver-test.coffee
// Copyright 2015, 2016 Evan Prodromou <evan@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const templateBatch = require('./template-batch')

const EMAIL = 'already-a-user@unit.test'

const props = {
  to: EMAIL,
  subject: 'You are already a user',
  data: {
    email: EMAIL
  }
}

vows
  .describe('POST /already-a-user')
  .addBatch(templateBatch('already-a-user', props, {
    'the login URL is included' (err, message, body) {
      assert.ifError(err)
      const rx = new RegExp('http://fuzzyio.test:1516/login')
      return assert.match(message != null ? message.data : undefined, rx)
    }
  }
  )).export(module)
