// mailerserver-test.coffee
// Copyright 2015, 2016 Evan Prodromou <evan@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const templateBatch = require('./template-batch')

const EMAIL = 'no-need-to-register@unit.test'

const props = {
  to: EMAIL,
  subject: 'No need to sign up for Fuzzy.ai',
  data: {
    email: EMAIL
  }
}

vows
  .describe('POST /no-need-to-register')
  .addBatch(templateBatch('no-need-to-register', props, {
    'the login URL is included' (err, message, body) {
      assert.ifError(err)
      const rx = new RegExp('http://fuzzyio.test:1516/login')
      return assert.match(message != null ? message.data : undefined, rx)
    }
  }
  )).export(module)
