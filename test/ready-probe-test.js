// ready-probe-test.coffee
// Test that the Kubernetes ready probe endpoint is responsive
// Copyright 2017 Fuzzy.ai <legal@fuzzy.ai>
// All rights reserved

const vows = require('perjury')
const { assert } = vows
const request = require('request')

const apiBatch = require('./api-batch')

vows.describe('GET /ready')
  .addBatch(apiBatch({
    'and we get the /ready endpoint': {
      topic () {
        request.get('http://localhost:2342/ready', this.callback)
        return undefined
      },
      'it works' (err, response, body) {
        assert.ifError(err)
        assert.isObject(response)
        assert.equal(response.statusCode, 200)
        assert.isString(body)
        return assert.equal(JSON.parse(body).status, 'OK')
      }
    }
  })).export(module)
