// live-probe-test.coffee
// Test that the Kubernetes live probe endpoint is responsive
// Copyright 2017 Fuzzy.ai <legal@fuzzy.ai>
// All rights reserved

const vows = require('perjury')
const { assert } = vows
const request = require('request')

const apiBatch = require('./api-batch')

vows.describe('GET /live')
  .addBatch(apiBatch({
    'and we get the /live endpoint': {
      topic () {
        request.get('http://localhost:2342/live', this.callback)
        return undefined
      },
      'it works' (err, response, body) {
        assert.ifError(err)
        assert.isObject(response)
        assert.equal(response.statusCode, 200)
        assert.isString(body)
        return assert.equal(JSON.parse(body).status, 'OK')
      }
    }
  })).export(module)
