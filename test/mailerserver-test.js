// mailerserver-test.coffee
// Copyright 2015 Evan Prodromou <evan@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')

vows
  .describe('Mailer server')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const MailerServer = require('../lib/mailerserver')
          callback(null, MailerServer)
        } catch (err) {
          callback(err)
        }
        return undefined
      },
      'it works' (err, MailerServer) {
        return assert.ifError(err)
      },
      'it is a class' (err, MailerServer) {
        assert.ifError(err)
        return assert.isFunction(MailerServer)
      },
      'and we instantiate an MailerServer': {
        topic (MailerServer) {
          const { callback } = this
          try {
            const env = {
              PORT: 2342,
              HOSTNAME: 'localhost',
              LOG_FILE: '/dev/null'
            }
            const server = new MailerServer(env)
            callback(null, server)
          } catch (err) {
            callback(err)
          }
          return undefined
        },
        'it works' (err, server) {
          return assert.ifError(err)
        },
        'it is an object' (err, server) {
          assert.ifError(err)
          return assert.isObject(server)
        },
        'it has a start() method' (err, server) {
          assert.ifError(err)
          assert.isObject(server)
          return assert.isFunction(server.start)
        },
        'it has a stop() method' (err, server) {
          assert.ifError(err)
          assert.isObject(server)
          return assert.isFunction(server.stop)
        },
        'and we start the server': {
          topic (server) {
            const { callback } = this
            server.start((err) => {
              if (err) {
                return callback(err)
              } else {
                return callback(null)
              }
            })
            return undefined
          },
          'it works' (err) {
            return assert.ifError(err)
          },
          'and we request the version': {
            topic () {
              const { callback } = this
              const url = 'http://localhost:2342/version'
              request.get(url, (err, response, body) => {
                const code = response != null ? response.statusCode : undefined
                if (err) {
                  return callback(err)
                } else if ((code >= 400) && (code <= 599)) {
                  return callback(new Error(`Bad status code ${code}`))
                } else {
                  return callback(null, JSON.parse(body))
                }
              })
              return undefined
            },
            'it works' (err, version) {
              return assert.ifError(err)
            },
            'it looks correct' (err, version) {
              assert.ifError(err)
              assert.isObject(version)
              assert.include(version, 'version')
              return assert.include(version, 'name')
            },
            'and we stop the server': {
              topic (version, server) {
                const { callback } = this
                server.stop((err) => {
                  if (err) {
                    return callback(err)
                  } else {
                    return callback(null)
                  }
                })
                return undefined
              },
              'it works' (err) {
                return assert.ifError(err)
              },
              'and we request the version': {
                topic () {
                  const { callback } = this
                  const url = 'http://localhost:2342/version'
                  request.get(url, (err, response, body) => {
                    if (err) {
                      return callback(null)
                    } else {
                      return callback(new Error('Unexpected success after server stop'))
                    }
                  })
                  return undefined
                },
                'it fails correctly' (err) {
                  return assert.ifError(err)
                }
              }
            }
          }
        }
      }
    }}).export(module)
