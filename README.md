mailer
======

This is the mailer microservice. It's used to send templated emails to users or
staff.

The templates are in the directory mail-src/ if you need to change them or
add a new one.

Environment
-----------

This microservice extends Microservice, so it uses all the environment variables
for that service. In addition, it uses the following extra variables:

* MAIL_SOURCE: The directory where the mail template are. I think this
  is for testing, but I'm not sure.
* MAIL_HOST: Host for sending outgoing mail.
* MAIL_PORT: Port for sending outgoing mail.
* MAIL_USERNAME: User to use for logging into the SMTP server.
* MAIL_PASSWORD: Password to use for logging into the SMTP server.
* MAIL_FROM: Default "From:" for emails.
* HOME_HOST: For outgoing mail that needs to include a link to the
  home site, this is the hostname part of the home site URL. Usually something
  like "beta.fuzzy.ai" or "dev.fuzzy.ai" or "fuzzy.ai" or "localhost".
* HOME_PORT: Port for the home site URL. Usually 80 or 443 will get
  changed correctly. Set this if you're running locally on an off-80 port.

Authorization
-------------

All the endpoints use the app key authorization used in Microservice. So, to add
a new client, add an environment variable like APP_KEY_SOMETHING, where
SOMETHING is the name of the client.

Endpoints
---------

The server currently supports one endpoint per template. Each endpoint accepts
JSON payloads.

* POST /confirmation

  * to: the email address to send the confirmation to
  * subject: Subject of the email
  * data: Data for the template. An object. Should include:
    * confirmation: a confirmation object, including
      * code: A confirmation code

  Send an email confirmation message, after initial signup, including a link to
  the home site to confirm the email address exists.

  Returns a JSON object:

  * status: Always "OK".
  * id: ID of the message posted. You can check the status.

* POST /error

  * to: the email address to send the error message to
  * subject: Subject of the email
  * data: Data for the error. An object. Should include:
    * timestamp: time the error happened
    * app: name of the app
    * hostname: host where the error happened
    * error: an object with at least the following properties. Others will be
      shown if they exist.
      * message: what went wrong
      * statusCode: HTTP status code
    * extra: any extra data, as object properties.

  Send an error email, so that admins know that there's a problem with the
  servers. We use Slack for this now, but some old code might still use this.
  Maybe.

  Returns a JSON object:

  * status: Always "OK".

* POST /invitation-request

  * to: the email address to send the invitation request
  * subject: Subject of the email
  * data: Data for the request. An object. Should include:
    * email: email address of the requesting user

  Send an invitation request, usually to admins.

  Returns a JSON object:

  * status: Always "OK".

* POST /reset

  * to: the email address to send the confirmation to
  * subject: Subject of the email
  * data: Data for the template. An object. Should include:
    * confirmation: a confirmation object, including
      * code: A confirmation code

  Send an email confirmation message, after password reset request, including a
  link to the home site to confirm the email address is in the control of the
  user.

  Returns a JSON object:

  * status: Always "OK".

* POST /test

  * to: the email address to send the test email to
  * subject: Subject of the email
  * data: Data for the template. An object. Should include:
    * item1: Some data
    * item2: Some other data

  This is just a test template.

  Returns a JSON object:

  * status: Always "OK".

* POST /already-a-user

  * to: the email address to send the test email to
  * subject: Subject of the email
  * data: Data for the template. An object. Should include:
    * email: email that is already a user

  If someone requests a coupon code and they're already a user. Yes, it's
  kind of specific.

  Returns a JSON object:

  * status: Always "OK".

* POST /no-need-to-register

  * to: the email address to send the test email to
  * subject: Subject of the email
  * data: Data for the template. An object. Should include:
    * email: email that is already a user

  If someone registers and they're already a user.

  Returns a JSON object:

  * status: Always "OK".

* GET /live

  Returns `{status: "OK"}` if the server is online.

* GET /ready

  Returns `{status: "OK"}` if the server is ready to take requests.
